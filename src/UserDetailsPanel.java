import javax.swing.*;
import java.awt.*;
import java.awt.event.HierarchyEvent;
import java.awt.event.HierarchyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created with IntelliJ IDEA.
 * User: Ben
 * Date: 19/08/2013
 * Time: 16:36
 * To change this template use File | Settings | File Templates.
 */
public class UserDetailsPanel {
    private final String username;
    private final String password;

    public UserDetailsPanel() {
        //Create a panel that will be use to put
        //one JTextField, one JPasswordField and two JLabel
        JPanel panel=new JPanel();

        //Set JPanel layout using GridLayout
        panel.setLayout(new GridLayout(4,1));

        //Create a label with text (Username)
        JLabel username=new JLabel("Username");

        //Create a label with text (Password)
        JLabel password=new JLabel("Password");

        //Create text field that will use to enter username
        JTextField textField=new JTextField(20);
        textField.addHierarchyListener( new RequestFocusListener() );

        //Create password field that will be use to enter password
        JPasswordField passwordField=new JPasswordField(20);

        //Add label with text (username) into created panel
        panel.add(username);

        //Add text field into created panel
        panel.add(textField);

        //Add label with text (password) into created panel
        panel.add(password);

        //Add password field into created panel
        panel.add(passwordField);

        //Show JOptionPane that will ask user for username and password
        int a=JOptionPane.showConfirmDialog(null,panel,"Enter username and password",JOptionPane.OK_CANCEL_OPTION,JOptionPane.QUESTION_MESSAGE);

        //Operation that will do when user click 'OK'
        if(a==JOptionPane.OK_OPTION)
        {
            this.username = textField.getText();
            this.password = String.valueOf(passwordField.getPassword());
        }
        else
        {
            this.username = "";
            this.password = "";
        }
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}

class RequestFocusListener implements HierarchyListener {

    @Override
    public void hierarchyChanged(HierarchyEvent e) {
        final Component c = e.getComponent();
        if (c.isShowing() && (e.getChangeFlags() & HierarchyEvent.SHOWING_CHANGED) != 0) {
            Window toplevel = SwingUtilities.getWindowAncestor(c);
            toplevel.addWindowFocusListener(new WindowAdapter() {

                @Override
                public void windowGainedFocus(WindowEvent e) {
                    c.requestFocus();
                }
            });
        }
    }
}

