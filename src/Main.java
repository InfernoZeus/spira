import com.inflectra.spirateam.mylyn.core.internal.services.soap.*;
import com.sun.org.apache.xerces.internal.jaxp.datatype.XMLGregorianCalendarImpl;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * User: Ben
 * Date: 02/08/13
 * Time: 18:55
 */
public class Main {

    static final String USERNAME = "";
    static final String PASSWORD = "";

    static final String BASE_URL = "https://accelleran.spiraservice.net";
    static final String WEB_SERVICE_SUFFIX = "/Services/v4_0/ImportExport.svc"; //$NON-NLS-1$
    static final String WEB_SERVICE_NAMESPACE = "{http://www.inflectra.com/SpiraTest/Services/v4.0/}ImportExport"; //$NON-NLS-1$
    static final String WEB_SERVICE_NAMESPACE_DATA_OBJECTS = "http://schemas.datacontract.org/2004/07/Inflectra.SpiraTest.Web.Services.v4_0.DataObjects"; //$NON-NLS-1$
    static final String PROJECT_NAME = "Library Information System";
    static ObjectFactory FACTORY = null;

    public static void main(String[] argv) {

        IImportExport soap = connectToService();
        if (soap != null) {
            RemoteProject project = selectProject(soap);
            if (project != null) {
                System.out.println("Using project " + project.getName().getValue());
                Integer testCaseId = 16;
                try {
                    GregorianCalendar start = new GregorianCalendar();
                    start.add(Calendar.MINUTE, -5);
                    GregorianCalendar end = new GregorianCalendar();
                    DatatypeFactory datatypeFactory = DatatypeFactory.newInstance();
                    XMLGregorianCalendar xmlStart = datatypeFactory.newXMLGregorianCalendar((GregorianCalendar) start);
                    XMLGregorianCalendar xmlEnd = datatypeFactory.newXMLGregorianCalendar((GregorianCalendar) end);

                    Integer executionStatusId = 0;
                    String runnerName = "runnerName2";
                    Integer runnerAssertCount = 1;
                    Integer testRunFormatId = 1;

                    RemoteAutomatedTestRun testRun = new RemoteAutomatedTestRun();
                    testRun.setTestCaseId(testCaseId);
                    testRun.setStartDate(xmlStart);
                    testRun.setEndDate(FACTORY.createRemoteTestRunEndDate(xmlEnd));
                    testRun.setExecutionStatusId(executionStatusId);
                    testRun.setRunnerName(FACTORY.createRemoteAutomatedTestRunRunnerName(runnerName));
                    testRun.setRunnerAssertCount(FACTORY.createRemoteAutomatedTestRunRunnerAssertCount(runnerAssertCount));
                    testRun.setTestRunFormatId(testRunFormatId);

                    soap.testRunRecordAutomated1(testRun);
                } catch (IImportExportTestRunRecordAutomated1ValidationFaultMessageFaultFaultMessage iImportExportTestRunRecordAutomated1ValidationFaultMessageFaultFaultMessage) {
                    iImportExportTestRunRecordAutomated1ValidationFaultMessageFaultFaultMessage.printStackTrace();
                } catch (IImportExportTestRunRecordAutomated1ServiceFaultMessageFaultFaultMessage iImportExportTestRunRecordAutomated1ServiceFaultMessageFaultFaultMessage) {
                    iImportExportTestRunRecordAutomated1ServiceFaultMessageFaultFaultMessage.printStackTrace();
                } catch (DatatypeConfigurationException e) {
                    e.printStackTrace();
                }
//                HashMap<String, RemoteTestCase> testCases = listTestCases(soap);
//                if (testCases != null) {
//                    for (RemoteTestCase testCase: testCases.values()) {
//                        System.out.println(testCase.getName().getValue());
//                    }
//                }
            }
        }

    }

    private static IImportExport connectToService() {
        try {
            URL wsdlLocation = new URL(BASE_URL + WEB_SERVICE_SUFFIX);
            QName serviceName = QName.valueOf(WEB_SERVICE_NAMESPACE);
            ImportExport service = new ImportExport(wsdlLocation, serviceName);

            IImportExport soap = service.getBasicHttpBindingIImportExport();

            //Make sure that session is maintained
            Map<String, Object> requestContext = ((BindingProvider)soap).getRequestContext();
            requestContext.put(BindingProvider.SESSION_MAINTAIN_PROPERTY,true);

//            UserDetailsPanel details = new UserDetailsPanel();

            boolean success = soap.connectionAuthenticate(USERNAME, PASSWORD);
            if (success) {
                FACTORY = new ObjectFactory();
                return soap;
            } else
                System.out.println("Invalid username/password");
                return null;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IImportExportConnectionAuthenticateServiceFaultMessageFaultFaultMessage iImportExportConnectionAuthenticateServiceFaultMessageFaultFaultMessage) {
            iImportExportConnectionAuthenticateServiceFaultMessageFaultFaultMessage.printStackTrace();
        }
        return null;
    }

    private static RemoteProject selectProject(IImportExport soap) {
        try {
            ArrayOfRemoteProject projects = soap.projectRetrieve();
            for (RemoteProject project : projects.getRemoteProject()) {
                if (project.getName().getValue().equals(PROJECT_NAME)) {
                    soap.connectionConnectToProject(project.getProjectId().getValue());
                    return project;
                }
            }
        } catch (IImportExportProjectRetrieveServiceFaultMessageFaultFaultMessage iImportExportProjectRetrieveServiceFaultMessageFaultFaultMessage) {
            iImportExportProjectRetrieveServiceFaultMessageFaultFaultMessage.printStackTrace();
        } catch (IImportExportConnectionConnectToProjectServiceFaultMessageFaultFaultMessage iImportExportConnectionConnectToProjectServiceFaultMessageFaultFaultMessage) {
            iImportExportConnectionConnectToProjectServiceFaultMessageFaultFaultMessage.printStackTrace();
        }
        return null;
    }

    private static HashMap<String, RemoteTestCase> listTestCases(IImportExport soap) {
        try {
            HashMap<String, RemoteTestCase> testCases = new HashMap();
            int count = 1;
            boolean reachedEnd = false;
            do {
                ArrayOfRemoteTestCase resultTestCases = soap.testCaseRetrieve(null, count, 5);
                for (RemoteTestCase testCase : resultTestCases.getRemoteTestCase()) {
                    testCases.put(testCase.getName().getValue(), testCase);
                }
                if (resultTestCases.getRemoteTestCase().size() < 5) {
                    reachedEnd = true;
                } else {
                    count += 5;
                }
            } while (!reachedEnd);
            return testCases;
        } catch (IImportExportTestCaseRetrieveServiceFaultMessageFaultFaultMessage iImportExportTestCaseRetrieveServiceFaultMessageFaultFaultMessage) {
            iImportExportTestCaseRetrieveServiceFaultMessageFaultFaultMessage.printStackTrace();
        }
        return null;
    }

}